import React from "react";
import Home from "./Home";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
const SimpleForm = () => {
  const { register, handleSubmit, errors } = useForm();
  const history = useHistory();
  const onSubmit = (data) => {
    console.log(" On submitt clicked!", data);
    history.push("/home");
  };

  return (
    <div className="card card-primary align-items-center col-md-6">
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* <label className="text-bold">Username*</label> */}
        <input
          type="text"
          name="username"
          ref={register({ required: true })}
          placeholder="Username*"
          className="form-control m-2"
        />
        {errors.username && (
          <small className="text-danger">Username is required</small>
        )}
        {/* <label className="text-bold">Password*</label> */}
        <input
          type="password"
          name="password"
          ref={register({ required: true })}
          placeholder="Password*"
          className="form-control m-2"
        />
        {errors.password && (
          <small className="text-danger">
            Password is required
            <br />
          </small>
        )}
        <button className="btn btn-success m-2" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
};

export default SimpleForm;
