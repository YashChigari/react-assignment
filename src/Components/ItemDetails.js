import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Dashboard(props) {
  useEffect(() => {
    fetchItems();
  }, []);

  const [item, setItems] = useState([]);

  const fetchItems = async () => {
    console.log("Props", props);
    const data = await fetch(
      "https://fortnite-api.theapinetwork.com/upcoming/get?id=" +
        props.match.params.id
    );
    const events = await data.json();
    console.log("Item details", events);
    // setItems(events.data);
  };

  return (
    <div className="align-items-center">
      <h1>Item Details Page</h1>
      <h4>{props.match.params.id}</h4>
    </div>
  );
}

export default Dashboard;
