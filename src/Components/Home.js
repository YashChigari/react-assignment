import React from "react";
import ScheduleMeeting from "./ScheduleMeeting";

function Home() {
  return (
    <div className="align-items-center">
      <h1 className="m-3">Home Page</h1>
      <ScheduleMeeting />
    </div>
  );
}

export default Home;
