import React from "react";
import SimpleForm from "./SimpleForm";
const Welcome = () => (
  <div className="">
    <h1>Welcome Page</h1>
    <div className="d-flex justify-content-center align-self-center">
      <SimpleForm />
    </div>
  </div>
);

export default Welcome;
