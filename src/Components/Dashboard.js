import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Dashboard() {
  useEffect(() => {
    fetchItems();
  }, []);

  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const data = await fetch(
      "https://fortnite-api.theapinetwork.com/upcoming/get"
    );
    const events = await data.json();
    console.log("Events", events);
    setItems(events.data);
  };

  return (
    <div className="align-items-center">
      <h1>Dashboard Page</h1>
      {items.map((item) => (
        <h3 item={item} key={item.itemId}>
          <Link to={`/dash/${item.itemId}`}>{item.item.name}</Link>
        </h3>
      ))}
    </div>
  );
}

export default Dashboard;
