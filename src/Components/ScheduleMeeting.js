import React, { useState } from "react";
import { useForm } from "react-hook-form";
// import { useHistory } from "react-router-dom";
import { createStore } from "redux";

const ScheduleMeeting = (props) => {
  const [participantName, setParticipantName] = useState("");
  const [participantEmail, setParticipantEmail] = useState("");
  const [date, setDate] = useState("");
  const [scheduledMeetings, setScheduledMeetings] = useState([]);

  const { register, handleSubmit, errors } = useForm();
  //   const history = useHistory();
  const onSubmit = (data) => {
    setScheduledMeetings((prevScheduledMeeting) => {
      return [...prevScheduledMeeting, data];
    });
    setParticipantEmail("");
    setParticipantName("");
    setDate("");
    console.log(" Scheduled Meetings", scheduledMeetings);
    // meeting.dispatch(addMeeting(data));
  };

  // const meeting = createStore(meetings, []);

  // const addMeeting = () => {
  //   return {
  //     type: "ADD_MEETING",
  //     meeting,
  //   };
  // };

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="col-12 row">
          <div className="col-md-3 mb-3">
            <label>Participant's name</label>
            <input
              type="text"
              name="participantName"
              value={participantName}
              onChange={(e) => {
                setParticipantName(e.target.value);
              }}
              ref={register({ required: true })}
              placeholder={
                errors.participantName
                  ? "Participant's name is required"
                  : "Participant's name*"
              }
              className={
                "form-control " + (errors.participantName ? "is-invalid" : "")
              }
            />
            <div className="valid-feedback">Looks good!</div>
          </div>
          <div className="col-md-3 mb-3">
            <label>Participant's email</label>
            <input
              type="text"
              name="participantEmail"
              value={participantEmail}
              onChange={(e) => setParticipantEmail(e.target.value)}
              ref={register({ required: true })}
              placeholder={
                errors.participantName
                  ? "Participant's email is required"
                  : "Participant's email*"
              }
              className={
                "form-control " + (errors.participantName ? "is-invalid" : "")
              }
            />{" "}
            <div className="valid-feedback">Looks good!</div>
          </div>
          <div className="col-md-3 mb-3">
            <label>Date*</label>
            <input
              type="text"
              name="date"
              value={date}
              onChange={(e) => setDate(e.target.value)}
              ref={register({ required: true })}
              placeholder={
                errors.date
                  ? "Date is required : dd-mm-yyy"
                  : "Date : dd-mm-yyy*"
              }
              className={"form-control " + (errors.date ? "is-invalid" : "")}
            />
            <div className="valid-feedback">Looks good!</div>
          </div>
          <button type="submit" className="btn btn-success col-md-3 btn-block">
            Submit
          </button>
        </div>
      </form>
      <hr />
      <div>
        <h3 className="text-success">Scheduled Meetings</h3>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Participant's name</th>
              <th scope="col">Participant's email</th>
              <th scope="col">Date</th>
            </tr>
          </thead>
          <tbody>
            {scheduledMeetings.map((item) => {
              return (
                <tr>
                  <td>{item.participantName}</td>
                  <td>{item.participantEmail}</td>
                  <td>{new Date(item.date).toDateString()}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ScheduleMeeting;
