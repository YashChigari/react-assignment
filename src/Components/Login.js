import React from "react";
import SimpleForm from "./SimpleForm";
const Login = () => (
  <div className="">
    <h1>Login Page</h1>
    <div className="d-flex justify-content-center align-self-center">
      <SimpleForm />
    </div>
  </div>
);

export default Login;
