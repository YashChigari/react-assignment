import React from "react";
import "./App.css";
import Nav from "./Components/Nav";
import Home from "./Components/Home";
import Login from "./Components/Login";
import Welcome from "./Components/Welcome";
import Dashboard from "./Components/Dashboard";
import ItemDetails from "./Components/ItemDetails";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SimpleForm from "./Components/SimpleForm";

function App() {
  return (
    <Router>
      <div className="app text-center">
        <Nav />
        <Switch>
          <Route path="/" exact component={Login}></Route>
          <Route path="/home" component={Home}></Route>
          <Route path="/login" component={Login}></Route>
          <Route path="/dash" exact component={Dashboard}></Route>
          <Route path="/dash/:id" component={ItemDetails}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
